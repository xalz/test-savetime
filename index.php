<?php

require_once __DIR__ . '/libs/FileUpload/Upload.php';
require_once __DIR__ . '/libs/FileUpload/Base.php';
require_once __DIR__ . '/libs/FileUpload/FIle.php';

use FileUpload\Upload;
use FileUpload\File;
use FileUpload\Base;

$host = '127.0.0.1';
$db   = 'test';
$user = 'root';
$pass = '';
$charset = 'utf8';

$config = [
    'mimeTypes' => [ 'image/png', 'image/gif', 'image/jpeg' ],
    'uploadDir' => __DIR__ . DIRECTORY_SEPARATOR . 'uploads'
];

$files = [];
$base = new Base($host, $db, $user, $pass, $charset);
$uploader = new Upload($base, $config);

if(!empty($_GET['id']) && $_GET['id']){
    $files = [ $uploader->info($_GET['id']) ];
}

if(!empty($_FILES) && is_array($_FILES)){
    $files = array_map(function($item) use ($uploader, $base) {
        $file = new File($item);
        if(!$file->get('error')){
            return $uploader->upload($file);
        }
        return null;
    }, $_FILES);
}


?>

<form method="POST" enctype="multipart/form-data">
    <div><input name="file_1" type="file"></div>
    <div><input name="file_2" type="file"></div>
    <div><input name="file_3" type="file"></div>
    <div><button>Upload!</button></div>
</form>

<?php foreach ($files as $file): ?>
    <?php if($file instanceof File): ?>
        <pre><?php var_dump($file->toArray()); ?></pre>
    <?php endif; ?>
<?php endforeach; ?>

