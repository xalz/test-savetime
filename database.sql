CREATE TABLE `files`
(
 `id`      INTEGER NOT NULL AUTO_INCREMENT ,
 `name`    VARCHAR(45) NOT NULL ,
 `type`    VARCHAR(45) NOT NULL ,
 `path`    VARCHAR(45) NOT NULL ,
 `size`    INT NOT NULL ,
 `created` DATETIME NOT NULL ,

PRIMARY KEY (`id`),
KEY `files_id` (`id`)
);