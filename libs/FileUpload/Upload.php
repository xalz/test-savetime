<?php

namespace FileUpload;

class Upload {

    protected $db;
    protected $config;

    const defaultConfig = [
        'uploadDir' => null,
        'maxSize' => 1024 * 1000,
        'mimeTypes' => []
    ];

    public function __construct(Base $db, array $config = []) {
        $config = array_merge($this::defaultConfig, $config);
        $this->config = new \ArrayObject($config, \ArrayObject::ARRAY_AS_PROPS);
        $this->db = $db;
    }

    public function upload(File $file){

        if($error = $this->getError($file->get('error'))){
            throw new \Exception($error);
        }
        if(!$this->validateMimeType($file->get('type'))){
            throw new \Exception('File type ' . $file->get('type') . ' not allowed');
        }
        if(!$this->validateSize($file->get('size'))){
            throw new \Exception('File size ' . $file->get('size') . ' not allowed');
        }

        $file->set('path', $this->getPath($file));
        $file->set('created', date('Y-m-d H:i:s'));
        if(!$this->setFile($file) || !$id = $this->setEntity($file)){
            throw new \Exception('Cant save file');
        }

        $file->set('id', $id);
        return $file;
    }

    public function info($id){
        if(!$file = $this->getEntity($id)){
            throw new \Exception('File not found');
        }
        return new File($file);
    }

    protected function validateMimeType($type){
        return in_array((string) $type, (array) $this->config->mimeTypes);
    }

    protected function validateSize($size){
        return $this->config->maxSize >= (int) $size;
    }

    protected function setEntity(File $file){
        return $this->db->save('files', $file->toArray());
    }

    protected function setFile(File $file){
        $file->set('path', $this->getPath($file));
        return move_uploaded_file($file->get('tmp_name'), $file->get('path'));
    }

    protected function getPath(File $file){
        $path = pathinfo($file->get('name'));
        return join(DIRECTORY_SEPARATOR, [
            $this->config->uploadDir,
            join('.', [ sha1_file($file->get('tmp_name')), $path['extension'] ])
        ]);
    }

    protected function getEntity($id){
        return $this->db->findById('files', $id);
    }

    protected function getError($error){
        switch ($error) {
            case UPLOAD_ERR_OK:
                return null;

            case UPLOAD_ERR_NO_FILE:
                return 'No file sent';

            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                return 'Exceeded file size limit';

            default:
                return 'Unknown errors';
        }
    }


}